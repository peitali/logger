package logger

import (
	"testing"
)

func TestLevel(t *testing.T) {
	t.Log(Log)
	Log.SetLevel(LevelError)
	t.Log(Log)
}

func TestConfig(t *testing.T) {
	c := map[string]any{
		"filename": "./logs/log.log",
		"level":    LevelError,
	}
	InitNewLogger(c)
	t.Logf("%#v", Log)
	Log.Info("asdf")
	Log.Error("abcd")
}
