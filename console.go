package logger

import (
	"encoding/json"
	"io"
	"os"
	"sync"
	"time"
)

func init() {
	Register(AdapterConsole, NewConsole)
}

type consoleWriter struct {
	sync.Mutex
	w     io.Writer
	Level int  `json:"level"`
	Color bool `json:"color"`
}

func NewConsole() Logger {
	return &consoleWriter{
		w:     os.Stdout,
		Level: LevelDebug,
		Color: true,
	}
}

func (c *consoleWriter) Init(config string) error {
	if len(config) == 0 {
		return nil
	}
	return json.Unmarshal([]byte(config), c)
}

func (c *consoleWriter) Write(when time.Time, level int, msg string) error {
	if level > c.Level {
		return nil
	}

	c.Lock()
	defer c.Unlock()

	w := when.Format("2006-01-02 15:04:05.999 ")
	_, err := c.w.Write(append(append([]byte(w), msg...), '\n'))
	return err
}

func (c *consoleWriter) Name() string {
	return AdapterConsole
}
