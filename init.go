package logger

import (
	"encoding/json"
)

var Log *EsonLogger

func init() {
	t := map[string]any{
		"filename": "./logs/log.log",
	}

	Log = InitDefaultNewLogger(t)
}

func InitDefaultNewLogger(config map[string]any) *EsonLogger {
	l := NewLogger()
	data, _ := json.Marshal(config)
	l.SetLogger(AdapterFile, string(data))
	l.EnableTrace(true, 2)
	l.SetPrefix("eson")
	return l
}

func InitNewLogger(config map[string]any) {
	Log = InitDefaultNewLogger(config)
}
