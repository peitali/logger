package logger

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"strconv"
	"sync"
	"time"
)

// RFC5424 log levels
const (
	LevelEmergency = iota
	LevelAlert
	LevelCritical
	LevelError
	LevelWarning
	LevelNotice
	LevelInformational
	LevelDebug
)
const (
	LevelFatal = -1
)

const (
	AdapterConsole = "console"
	AdapterFile    = "file"
)

type LoggerFunc func() Logger

var adapters = make(map[string]LoggerFunc)
var levelPrefix = [9]string{"[F]", "[M]", "[A]", "[C]", "[E]", "[W]", "[N]", "[I]", "[D]"}

type EsonLogger struct {
	mu         sync.Mutex
	level      int
	trace      bool
	traceDepth int
	prefix     string
	outputs    []Logger
}

type Logger interface {
	Init(config string) error
	Write(when time.Time, level int, msg string) error
	Name() string
}

func NewLogger() *EsonLogger {
	el := new(EsonLogger)
	el.level = LevelDebug
	el.SetLogger(AdapterConsole, "")
	return el
}

func Register(name string, log LoggerFunc) {
	if log == nil {
		panic("logs: Register provide is nil")
	}
	if _, ok := adapters[name]; ok {
		panic("logs: Register log is exists")
	}
	adapters[name] = log
}

func (el *EsonLogger) SetLogger(name string, config string) error {
	if config == "" {
		config = "{}"
	}

	for _, v := range el.outputs {
		if v.Name() == name {
			return fmt.Errorf("logs: adaptername:%s is exists", name)
		}
	}

	adapter, ok := adapters[name]
	if !ok {
		return fmt.Errorf("logs: unknown adaptername %q (forgotten Register?)", name)
	}

	a := adapter()
	if err := a.Init(config); err != nil {
		return fmt.Errorf("logs: SetLogger: %s", err.Error())
	}
	el.outputs = append(el.outputs, a)
	return nil
}

func (el *EsonLogger) SetPrefix(s string) {
	el.prefix = s
}

func (el *EsonLogger) SetLevel(l int) {
	el.level = l
}

func (el *EsonLogger) EnableTrace(b bool, depth int) {
	el.trace = b
	el.traceDepth = depth
}

func (el *EsonLogger) write(level int, msg string, v ...interface{}) {
	if len(v) > 0 {
		msg = fmt.Sprintf(msg, v...)
	}
	msg = el.prefix + " " + msg

	if el.trace {
		_, file, line, ok := runtime.Caller(el.traceDepth)
		if !ok {
			file = "???"
			line = 0
		}
		_, filename := path.Split(file)
		//msg = "[ " + filename + ":" + strconv.Itoa(line) + " ] " + msg
		msg = filename + ":" + strconv.Itoa(line) + " " + msg
	}

	msg = levelPrefix[level+1] + " " + msg
	when := time.Now()
	//todo 异步日志

	el.writeToLoggers(when, level, msg)
}

func (el *EsonLogger) writeToLoggers(when time.Time, level int, msg string) {
	for _, v := range el.outputs {
		if err := v.Write(when, level, msg); err != nil {
			fmt.Fprintf(os.Stderr, "unable to WriteMsg to adapter:%v,error:%v", v.Name(), err.Error())
		}
	}
}

func (el *EsonLogger) Info(format string, v ...interface{}) {
	if LevelInformational > el.level {
		return
	}
	el.write(LevelInformational, format, v...)
}

func (el *EsonLogger) Emergency(format string, v ...interface{}) {
	if LevelEmergency > el.level {
		return
	}
	el.write(LevelEmergency, format, v...)
}

func (el *EsonLogger) Alert(format string, v ...interface{}) {
	if LevelAlert > el.level {
		return
	}
	el.write(LevelAlert, format, v...)
}

func (el *EsonLogger) Critical(format string, v ...interface{}) {
	if LevelCritical > el.level {
		return
	}
	el.write(LevelCritical, format, v...)
}

func (el *EsonLogger) Error(format string, v ...interface{}) {
	if LevelError > el.level {
		return
	}
	el.write(LevelError, format, v...)
}

func (el *EsonLogger) Warning(format string, v ...interface{}) {
	if LevelWarning > el.level {
		return
	}
	el.write(LevelWarning, format, v...)
}

func (el *EsonLogger) Notice(format string, v ...interface{}) {
	if LevelNotice > el.level {
		return
	}
	el.write(LevelNotice, format, v...)
}

func (el *EsonLogger) Informational(format string, v ...interface{}) {
	if LevelInformational > el.level {
		return
	}
	el.write(LevelInformational, format, v...)
}

func (el *EsonLogger) Debug(format string, v ...interface{}) {
	if LevelDebug > el.level {
		return
	}
	el.write(LevelDebug, format, v...)
}

func (el *EsonLogger) Fatal(format string, v ...interface{}) {
	if LevelFatal > el.level {
		return
	}
	el.write(LevelFatal, format, v...)
	os.Exit(1)
}
