package logger

import (
	"encoding/json"
	"errors"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

func init() {
	Register(AdapterFile, NewFileWriter)
}

type fileWriter struct {
	sync.RWMutex
	f        *os.File
	Filename string `json:"filename"`
	suffix   string

	Level int    `json:"level"`
	Perm  string `json:"perm"`
}

func NewFileWriter() Logger {
	return &fileWriter{
		Level: LevelDebug,
		Perm:  "0600",
	}
}

func (w *fileWriter) Init(config string) error {
	if len(config) == 0 {
		return nil
	}
	if err := json.Unmarshal([]byte(config), w); err != nil {
		return err
	}
	if w.Filename == "" {
		return errors.New("config must have filename")
	}
	w.suffix = filepath.Ext(w.Filename)
	if w.suffix == "" {
		w.suffix = ".log"
	}
	return w.startLogger()
}

func (w *fileWriter) startLogger() error {
	file, err := w.createLogFile()
	if err != nil {
		return err
	}
	if w.f != nil {
		w.f.Close()
	}
	w.f = file
	return w.initFd()
}

func (w *fileWriter) Name() string {
	return AdapterFile
}

func (w *fileWriter) Write(when time.Time, level int, msg string) error {
	if level > w.Level {
		return nil
	}
	t := when.Format("2006-01-02 15:04:05.999 ")
	msg = t + msg

	w.Lock()
	_, err := w.f.Write(append([]byte(msg), '\n'))
	w.Unlock()
	return err
}

func (w *fileWriter) createLogFile() (*os.File, error) {
	// Open the log file
	perm, err := strconv.ParseInt(w.Perm, 8, 64)
	if err != nil {
		return nil, err
	}

	fpath := path.Dir(w.Filename)
	os.MkdirAll(fpath, os.FileMode(perm))

	fd, err := os.OpenFile(w.Filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, os.FileMode(perm))
	if err == nil {
		os.Chmod(w.Filename, os.FileMode(perm))
	}
	return fd, err
}

func (w *fileWriter) initFd() error {
	//fd := w.f
	//fInfo, err := fd.Stat()
	//if err != nil {
	//	return fmt.Errorf("get stat err: %s", err)
	//}
	//w.maxSizeCurSize = int(fInfo.Size())
	//w.dailyOpenTime = time.Now()
	//w.dailyOpenDate = w.dailyOpenTime.Day()
	//w.hourlyOpenTime = time.Now()
	//w.hourlyOpenDate = w.hourlyOpenTime.Hour()
	//w.maxLinesCurLines = 0
	//if w.Hourly {
	//	go w.hourlyRotate(w.hourlyOpenTime)
	//} else if w.Daily {
	//	go w.dailyRotate(w.dailyOpenTime)
	//}
	//if fInfo.Size() > 0 && w.MaxLines > 0 {
	//	count, err := w.lines()
	//	if err != nil {
	//		return err
	//	}
	//	w.maxLinesCurLines = count
	//}
	return nil
}
